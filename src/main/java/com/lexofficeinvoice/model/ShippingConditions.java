package com.lexofficeinvoice.model;

public class ShippingConditions {
    private String shippingDate;
    private String shippingEndDate;
    public enum ShippingType{service,serviceperiod,delivery,deliveryperiod,none}

    private ShippingType shippingType ;

    public ShippingType getShippingType() {
        return shippingType;
    }

    public void setShippingType(ShippingType shippingType) {
        this.shippingType = shippingType;
    }

    public String getShippingDate() {
        return shippingDate;
    }

    public void setShippingDate(String shippingDate) {
        this.shippingDate = shippingDate;
    }

    public String getShippingEndDate() {
        return shippingEndDate;
    }

    public void setShippingEndDate(String shippingEndDate) {
        this.shippingEndDate = shippingEndDate;
    }

    @Override
    public String toString() {
        return "ShippingConditions{" +
                "shippingDate=" + shippingDate +
                ", shippingEndDate=" + shippingEndDate +
                ", shippingType=" + shippingType +
                '}';
    }
}
