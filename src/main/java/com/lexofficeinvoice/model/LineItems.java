package com.lexofficeinvoice.model;

import java.util.UUID;

public class LineItems {

    private UUID id;
    public enum Type{service, material,custom,text}
    private String name;
    private String description;
    private int quantity;
    private String unitName;
    private UnitPrice unitPrice;
    private int discountPercentage;
    private int lineItemAmount;
    private Type type;

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public UUID getId() {
        return id;
    }

    public void setId(UUID id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }

    public String getUnitName() {
        return unitName;
    }

    public void setUnitName(String unitName) {
        this.unitName = unitName;
    }

    public UnitPrice getUnitPrice() {
        return unitPrice;
    }

    public void setUnitPrice(UnitPrice unitPrice) {
        this.unitPrice = unitPrice;
    }

    public int getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(int discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public int getLineItemAmount() {
        return lineItemAmount;
    }

    public void setLineItemAmount(int lineItemAmount) {
        this.lineItemAmount = lineItemAmount;
    }

    @Override
    public String toString() {
        return "LineItems{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", description='" + description + '\'' +
                ", quantity=" + quantity +
                ", unitName='" + unitName + '\'' +
                ", unitPrice=" + unitPrice +
                ", discountPercentage=" + discountPercentage +
                ", lineItemAmount=" + lineItemAmount +
                '}';
    }
}
