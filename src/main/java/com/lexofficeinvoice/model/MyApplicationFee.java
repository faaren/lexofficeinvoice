package com.lexofficeinvoice.model;

import java.util.Currency;
import java.util.Date;

public class MyApplicationFee {

    private String id;
    private String object="application_fee";
    private String account;
    private int amount;
    private int amount_refunded;
    private String application;
    private String balance_transaction;
    private String charge;
    private Date created;
    private Currency currency;
    private boolean livemode;
    private String originating_transaction;
    private boolean refunded;

    public MyApplicationFee() {
    }

    private com.lexofficeinvoice.model.Refunds refunds;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getObject() {
        return object;
    }

    public void setObject(String object) {
        this.object = object;
    }

    public String getAccount() {
        return account;
    }

    public void setAccount(String account) {
        this.account = account;
    }

    public int getAmount() {
        return amount;
    }

    public void setAmount(int amount) {
        this.amount = amount;
    }

    public int getAmount_refunded() {
        return amount_refunded;
    }

    public void setAmount_refunded(int amount_refunded) {
        this.amount_refunded = amount_refunded;
    }

    public String getApplication() {
        return application;
    }

    public void setApplication(String application) {
        this.application = application;
    }

    public String getBalance_transaction() {
        return balance_transaction;
    }

    public void setBalance_transaction(String balance_transaction) {
        this.balance_transaction = balance_transaction;
    }

    public String getCharge() {
        return charge;
    }

    public void setCharge(String charge) {
        this.charge = charge;
    }

    public Date getCreated() {
        return created;
    }

    public void setCreated(Date created) {
        this.created = created;
    }

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public boolean isLivemode() {
        return livemode;
    }

    public void setLivemode(boolean livemode) {
        this.livemode = livemode;
    }

    public String getOriginating_transaction() {
        return originating_transaction;
    }

    public void setOriginating_transaction(String originating_transaction) {
        this.originating_transaction = originating_transaction;
    }

    public boolean isRefunded() {
        return refunded;
    }

    public void setRefunded(boolean refunded) {
        this.refunded = refunded;
    }

    public com.lexofficeinvoice.model.Refunds getRefunds() {
        return refunds;
    }

    public void setRefunds(com.lexofficeinvoice.model.Refunds refunds) {
        this.refunds = refunds;
    }


}
