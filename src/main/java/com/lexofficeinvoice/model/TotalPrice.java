package com.lexofficeinvoice.model;

public class TotalPrice {
    private String currency;
    private double totalNetAmount;
    private double totalGrossAmount;
    private double totalTaxAmount;
    //private double totalDiscountAbsolute;
    //private double totalDiscountPercentage;

    public TotalPrice() {

    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String currency) {
        this.currency = currency;
    }

    public double getTotalNetAmount() {
        return totalNetAmount;
    }

    public void setTotalNetAmount(double totalNetAmount) {
        this.totalNetAmount = totalNetAmount;
    }

    public double getTotalGrossAmount() {
        return totalGrossAmount;
    }

    public void setTotalGrossAmount(double totalGrossAmount) {
        this.totalGrossAmount = totalGrossAmount;
    }

    public double getTotalTaxAmount() {
        return totalTaxAmount;
    }

    public void setTotalTaxAmount(double totalTaxAmount) {
        this.totalTaxAmount = totalTaxAmount;
    }

 /*   public double getTotalDiscountAbsolute() {
        return totalDiscountAbsolute;
    }

    public void setTotalDiscountAbsolute(double totalDiscountAbsolute) {
        this.totalDiscountAbsolute = totalDiscountAbsolute;
    }

    public double getTotalDiscountPercentage() {
        return totalDiscountPercentage;
    }

    public void setTotalDiscountPercentage(double totalDiscountPercentage) {
        this.totalDiscountPercentage = totalDiscountPercentage;
    }*/

    @Override
    public String toString() {
        return "TotalPrice{" +
                "currency='" + currency + '\'' +
                ", totalNetAmount=" + totalNetAmount +
                ", totalGrossAmount=" + totalGrossAmount +
                ", totalTaxAmount=" + totalTaxAmount +
               // ", totalDiscountAbsolute=" + totalDiscountAbsolute +
                //", totalDiscountPercentage=" + totalDiscountPercentage +
                '}';
    }
}
