package com.lexofficeinvoice.model;

import java.util.UUID;

public class Address {
    private UUID contactId;
    private String name;
    private String supplement;
    private String street;
    private String city;
    private int zip;
    public enum CountryCode{DE,US,GB}
    private CountryCode countryCode;

/*    public Address(*//*UUID contactId,*//* String name, String supplement, String street, String city, int zip) {
      //  this.contactId = contactId;
        this.name = name;
        this.supplement = supplement;
        this.street = street;
        this.city = city;
        this.zip = zip;
    }*/

    public UUID getContactId() {
        return contactId;
    }

    public void setContactId(UUID contactId) {
        this.contactId = contactId;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSupplement() {
        return supplement;
    }

    public void setSupplement(String supplement) {
        this.supplement = supplement;
    }

    public String getStreet() {
        return street;
    }

    public void setStreet(String street) {
        this.street = street;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public int getZip() {
        return zip;
    }

    public void setZip(int zip) {
        this.zip = zip;
    }

    public CountryCode getCountryCode() {
        return countryCode;
    }

    public void setCountryCode(CountryCode countryCode) {
        this.countryCode = countryCode;
    }

    @Override
    public String toString() {
        return "Address{" +
                "contactId=" + contactId +
                ", name='" + name + '\'' +
                ", supplement='" + supplement + '\'' +
                ", street='" + street + '\'' +
                ", city='" + city + '\'' +
                ", zip=" + zip +
                ", countryCode=" + countryCode +
                '}';
    }
}
