package com.lexofficeinvoice.model;

import java.util.List;
import java.util.UUID;

public class LexInvoice {
    private String id;
    private UUID organizationId;
    private String createdDate;
    private String updatedDate;
    private int version;
    private String language;
    private boolean archived;

    public enum VoucherStatus {draft, open, paid, voided}

    private VoucherStatus voucherStatus;

    private String voucherNumber;
    private String voucherDate;
    private String dueDate;
    private com.lexofficeinvoice.model.Address address;
    List<com.lexofficeinvoice.model.LineItems> lineItems;
    private com.lexofficeinvoice.model.TotalPrice totalPrice;
    List<com.lexofficeinvoice.model.TaxAmount> taxAmounts;
    private com.lexofficeinvoice.model.TaxConditions taxConditions;
    private com.lexofficeinvoice.model.PaymentConditions paymentConditions;
    private com.lexofficeinvoice.model.ShippingConditions shippingConditions;

    private String title;
    private String introduction;
    private String remark;


    public LexInvoice(String id) {
        this.id = id;

    }

    public LexInvoice(String id, UUID organizationId, String createdDate, String updatedDate, int version, String language,
                      boolean archived, VoucherStatus voucherStatus, String voucherNumber, String voucherDate, String dueDate, com.lexofficeinvoice.model.Address address,
                      List<com.lexofficeinvoice.model.LineItems> lineItems, com.lexofficeinvoice.model.TotalPrice totalPrice, List<com.lexofficeinvoice.model.TaxAmount> taxAmounts, com.lexofficeinvoice.model.TaxConditions taxConditions,
                      com.lexofficeinvoice.model.PaymentConditions paymentConditions, com.lexofficeinvoice.model.ShippingConditions shippingConditions, String title, String introduction,
                      String remark, String files) {
        this.id = id;
        this.organizationId = organizationId;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.version = version;
        this.language = language;
        this.archived = archived;
        this.voucherStatus = voucherStatus;
        this.voucherNumber = voucherNumber;
        this.voucherDate = voucherDate;
        this.dueDate = dueDate;
        this.address = address;
        this.lineItems = lineItems;
        this.totalPrice = totalPrice;
        this.taxAmounts = taxAmounts;
        this.taxConditions = taxConditions;
        this.paymentConditions = paymentConditions;
        this.shippingConditions = shippingConditions;
        this.title = title;
        this.introduction = introduction;
        this.remark = remark;
        this.files = files;

    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public UUID getOrganizationId() {
        return organizationId;
    }

    public void setOrganizationId(UUID organizationId) {
        this.organizationId = organizationId;
    }

    public String getCreatedDate() {
        return createdDate;
    }

    public void setCreatedDate(String createdDate) {
        this.createdDate = createdDate;
    }

    public String getUpdatedDate() {
        return updatedDate;
    }

    public void setUpdatedDate(String updatedDate) {
        this.updatedDate = updatedDate;
    }

    public int getVersion() {
        return version;
    }

    public void setVersion(int version) {
        this.version = version;
    }

    public String getLanguage() {
        return language;
    }

    public void setLanguage(String language) {
        this.language = language;
    }

    public boolean isArchived() {
        return archived;
    }

    public void setArchived(boolean archived) {
        this.archived = archived;
    }

    public VoucherStatus getVoucherStatus(){return voucherStatus;}

    public void setVoucherStatus(VoucherStatus voucherStatus){this.voucherStatus=voucherStatus;}

    public String getVoucherNumber() {
        return voucherNumber;
    }

    public void setVoucherNumber(String voucherNumber) {
        this.voucherNumber = voucherNumber;
    }

    public String getVoucherDate() {
        return voucherDate;
    }

    public void setVoucherDate(String voucherDate) {
        this.voucherDate = voucherDate;
    }

    public String getDueDate() {
        return dueDate;
    }

    public void setDueDate(String dueDate) {
        this.dueDate = dueDate;
    }

    public com.lexofficeinvoice.model.Address getAddress() {
        return address;
    }

    public void setAddress(com.lexofficeinvoice.model.Address address) {
        this.address = address;
    }

    public List<com.lexofficeinvoice.model.LineItems> getLineItems() {
        return lineItems;
    }

    public void setLineItems(List<com.lexofficeinvoice.model.LineItems> lineItems) {
        this.lineItems = lineItems;
    }

    public com.lexofficeinvoice.model.TotalPrice getTotalPrice() {
        return totalPrice;
    }

    public void setTotalPrice(com.lexofficeinvoice.model.TotalPrice totalPrice) {
        this.totalPrice = totalPrice;
    }

    public List<com.lexofficeinvoice.model.TaxAmount> getTaxAmounts() {
        return taxAmounts;
    }

    public void setTaxAmounts(List<com.lexofficeinvoice.model.TaxAmount> taxAmounts) {
        this.taxAmounts = taxAmounts;
    }

    public com.lexofficeinvoice.model.TaxConditions getTaxConditions() {
        return taxConditions;
    }

    public void setTaxConditions(com.lexofficeinvoice.model.TaxConditions taxConditions) {
        this.taxConditions = taxConditions;
    }

    public com.lexofficeinvoice.model.PaymentConditions getPaymentConditions() {
        return paymentConditions;
    }

    public void setPaymentConditions(com.lexofficeinvoice.model.PaymentConditions paymentConditions) {
        this.paymentConditions = paymentConditions;
    }

    public com.lexofficeinvoice.model.ShippingConditions getShippingConditions() {
        return shippingConditions;
    }

    public void setShippingConditions(com.lexofficeinvoice.model.ShippingConditions shippingConditions) {
        this.shippingConditions = shippingConditions;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getIntroduction() {
        return introduction;
    }

    public void setIntroduction(String introduction) {
        this.introduction = introduction;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }

    public String getFiles() {
        return files;
    }

    public void setFiles(String files) {
        this.files = files;
    }

    String files;


    @Override
    public String toString() {
        return "LexInvoice{" +
                "id='" + id + '\'' +
                ", organizationId=" + organizationId +
                ", createdDate='" + createdDate + '\'' +
                ", updatedDate='" + updatedDate + '\'' +
                ", version=" + version +
                ", language='" + language + '\'' +
                ", archived=" + archived +
                ", voucherNumber='" + voucherNumber + '\'' +
                ", voucherDate='" + voucherDate + '\'' +
                ", dueDate='" + dueDate + '\'' +
                ", address=" + address +
                ", lineItems=" + lineItems +
                ", totalPrice=" + totalPrice +
                ", taxAmounts=" + taxAmounts +
                ", taxConditions=" + taxConditions +
                ", paymentConditions=" + paymentConditions +
                ", shippingConditions=" + shippingConditions +
                ", title='" + title + '\'' +
                ", introduction='" + introduction + '\'' +
                ", remark='" + remark + '\'' +
               // ", voucherStatus=" + voucherStatus +
                ", files='" + files + '\'' +
                '}';
    }
}
