package com.lexofficeinvoice.model;

public class TaxConditions
{
    public enum TaxType{net,gross,vatfree,intraCommunitySupply,constructionService13b,externalService13b,thirdPartyCountryService,thirdPartyCountryDelivery


    }
    private TaxType taxType;

    private String taxTypeNote;


    public TaxType getTaxType() {
        return taxType;
    }

    public void setTaxType(TaxType taxType) {
        this.taxType = taxType;
    }

    public String getTaxTypeNote() {
        return taxTypeNote;
    }

    public void setTaxTypeNote(String taxTypeNote) {
        this.taxTypeNote = taxTypeNote;
    }

    @Override
    public String toString() {
        return "TaxConditions{" +
                "taxTypeNote='" + taxTypeNote + '\'' +
                '}';
    }
}
