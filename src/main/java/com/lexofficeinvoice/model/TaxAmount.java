package com.lexofficeinvoice.model;

public class TaxAmount {
    private int taxRatePercentage;
    private double taxAmount;
    private double netAmount;

    public int getTaxRatePercentage() {
        return taxRatePercentage;
    }

    public void setTaxRatePercentage(int taxRatePercentage) {
        this.taxRatePercentage = taxRatePercentage;
    }

    public double getTaxAmount() {
        return taxAmount;
    }

    public void setTaxAmount(double taxAmount) {
        this.taxAmount = taxAmount;
    }

    public double getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(double netAmount) {
        this.netAmount = netAmount;
    }

    @Override
    public String toString() {
        return "TaxAmount{" +
                "taxRatePercentage=" + taxRatePercentage +
                ", taxAmount=" + taxAmount +
                ", netAmount=" + netAmount +
                '}';
    }
}
