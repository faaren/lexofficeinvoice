package com.lexofficeinvoice.model;

public class Refunds {

   private String object ;

   private boolean has_more;
   private String url;

   public String getObject() {
      return object;
   }

   public void setObject(String object) {
      this.object = object;
   }

   public boolean isHas_more() {
      return has_more;
   }

   public void setHas_more(boolean has_more) {
      this.has_more = has_more;
   }

   public String getUrl() {
      return url;
   }

   public void setUrl(String url) {
      this.url = url;
   }
}
