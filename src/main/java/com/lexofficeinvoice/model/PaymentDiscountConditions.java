package com.lexofficeinvoice.model;

public class PaymentDiscountConditions {
    private double discountPercentage;
    private double discountRange;

    public double getDiscountPercentage() {
        return discountPercentage;
    }

    public void setDiscountPercentage(double discountPercentage) {
        this.discountPercentage = discountPercentage;
    }

    public double getDiscountRange() {
        return discountRange;
    }

    public void setDiscountRange(double discountRange) {
        this.discountRange = discountRange;
    }
}
