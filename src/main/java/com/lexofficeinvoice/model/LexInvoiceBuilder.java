package com.lexofficeinvoice.model;

import java.util.List;
import java.util.UUID;

public class LexInvoiceBuilder {

    private String id;
    private UUID organizationId;
    private String createdDate;
    private String updatedDate;
    private int version;
    private String language;
    private boolean archived;

    private LexInvoice.VoucherStatus voucherStatus;
    private String voucherNumber;
    private String voucherDate;
    private String dueDate;
    private com.lexofficeinvoice.model.Address address;
    List<LineItems> lineItems;
    private com.lexofficeinvoice.model.TotalPrice totalPrice;
    List<TaxAmount> taxAmounts;
    private TaxConditions taxConditions;
    private PaymentConditions paymentConditions;
    private ShippingConditions shippingConditions;

    private String title;
    private String introduction;
    private String remark;
    private String files;


    public LexInvoiceBuilder(String id) {
    }

    public LexInvoiceBuilder(String id, UUID organizationId, String createdDate,
                             String updatedDate, int version, String language, boolean archived, LexInvoice.VoucherStatus voucherStatus,
                             String voucherNumber, String voucherDate, String dueDate, com.lexofficeinvoice.model.Address address, List<LineItems> lineItems, com.lexofficeinvoice.model.TotalPrice totalPrice,
                             List<TaxAmount> taxAmounts, TaxConditions taxConditions, PaymentConditions paymentConditions,
                             ShippingConditions shippingConditions, String title, String introduction, String remark, String files ) {
        this.id = id;
        this.organizationId = organizationId;
        this.createdDate = createdDate;
        this.updatedDate = updatedDate;
        this.version = version;
        this.language = language;
        this.archived = archived;
        this.voucherStatus = voucherStatus;
        this.voucherNumber = voucherNumber;
        this.voucherDate = voucherDate;
        this.dueDate = dueDate;
        this.address = address;
        this.lineItems = lineItems;
        this.totalPrice = totalPrice;
        this.taxAmounts = taxAmounts;
        this.taxConditions = taxConditions;
        this.paymentConditions = paymentConditions;
        this.shippingConditions = shippingConditions;
        this.title = title;
        this.introduction = introduction;
        this.remark = remark;
        this.files =files;

    }

   /* public LexInvoiceBuilder setId(UUID id) {
        this.id = id;
        return this;
    }*/

    public LexInvoiceBuilder setOrganizationId(UUID organizationId) {
        this.organizationId = organizationId;
        return this;

    }

    public LexInvoiceBuilder setCreatedDate(String createdDate){
        this.createdDate=createdDate;
        return this;
    }

    public LexInvoiceBuilder setUpdatedDate(String updatedDate){
        this.updatedDate=updatedDate;
        return this;
    }


    public LexInvoiceBuilder setVersion(int version){
        this.version=version;
        return this;
    }

    public LexInvoiceBuilder setLanguage(String language){
        this.language=language;
        return this;
    }

    public LexInvoiceBuilder setArchived(boolean archived){
        this.archived=archived;
        return this;
    }

    public LexInvoiceBuilder setVoucherStatus(LexInvoice.VoucherStatus voucherStatus){
        this.voucherStatus = voucherStatus;
        return this;
    }

    public LexInvoiceBuilder setVoucherNumber(String voucherNumber){
        this.voucherNumber=voucherNumber;
        return this;
    }

    public LexInvoiceBuilder setVoucherDate(String voucherDate){
        this.voucherDate=voucherDate;
        return this;
    }

    public LexInvoiceBuilder setDueDate(String dueDate){
        this.dueDate=dueDate;
        return this;

    }
    public LexInvoiceBuilder setAddress(com.lexofficeinvoice.model.Address address){
        this.address =address;
        return this;
    }
    public LexInvoiceBuilder setLineItems(List<LineItems> lineItems){
        this.lineItems=lineItems;
        return this;
    }
    public LexInvoiceBuilder setTotalPrice(com.lexofficeinvoice.model.TotalPrice totalPrice){
        this.totalPrice=totalPrice;
        return this;
    }

    public LexInvoiceBuilder setTaxAmounts(List<TaxAmount> taxAmounts){
        this.taxAmounts=taxAmounts;
        return this;
    }
    public LexInvoiceBuilder setTaxConditions(TaxConditions taxConditions){
        this.taxConditions=taxConditions;
        return this;
    }
    public LexInvoiceBuilder setPaymentConditions(PaymentConditions paymentConditions){
        this.paymentConditions=paymentConditions;
        return this;
    }
    public LexInvoiceBuilder setShippingConditions(ShippingConditions shippingConditions){
        this.shippingConditions=shippingConditions;
        return this;
    }
    public LexInvoiceBuilder setTitle(String title){
        this.title=title;
        return this;
    }
    public LexInvoiceBuilder setIntroduction(String introduction){
        this.introduction=introduction;
        return this;
    }
    public LexInvoiceBuilder setRemark(String remark){
        this.remark=remark;
        return this;
    }
    public LexInvoiceBuilder setFiles(String files){
        this.files=files;
        return this;
    }

    public LexInvoice build(){
        return new LexInvoice(id,organizationId,createdDate,updatedDate,version,language,archived,voucherStatus,voucherNumber,voucherDate,dueDate, address,lineItems,totalPrice,
                taxAmounts,taxConditions,paymentConditions,shippingConditions,title,introduction,remark,files);
    }

}
