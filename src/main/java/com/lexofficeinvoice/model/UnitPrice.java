package com.lexofficeinvoice.model;

public class UnitPrice {
    public enum Currency{EUR}
    private double netAmount;
    private double grossAmount;
    private int taxRatePercentage;

    private Currency currency;

    public Currency getCurrency() {
        return currency;
    }

    public void setCurrency(Currency currency) {
        this.currency = currency;
    }

    public double getNetAmount() {
        return netAmount;
    }

    public void setNetAmount(double netAmount) {
        this.netAmount = netAmount;
    }

    public double getGrossAmount() {
        return grossAmount;
    }

    public void setGrossAmount(double grossAmount) {
        this.grossAmount = grossAmount;
    }

    public int getTaxRatePercentage() {
        return taxRatePercentage;
    }

    public void setTaxRatePercentage(int taxRatePercentage) {
        this.taxRatePercentage = taxRatePercentage;
    }

    @Override
    public String toString() {
        return "UnitPrice{" +
                "netAmount=" + netAmount +
                ", grossAmount=" + grossAmount +
                ", taxRatePercentage=" + taxRatePercentage +
                '}';
    }
}
