package com.lexofficeinvoice.model;

import java.util.List;

public class PaymentConditions {
    private String paymentTermLabel;
    private int paymentTermDuration;
    List<com.lexofficeinvoice.model.PaymentDiscountConditions> paymentDiscountConditions;


    public PaymentConditions(){
    }

    public String getPaymentTermLabel() {
        return paymentTermLabel;
    }


    public void setPaymentTermLabel(String paymentTermLabel) {
        this.paymentTermLabel = paymentTermLabel;
    }

    public int getPaymentTermDuration() {
        return paymentTermDuration;
    }

    public void setPaymentTermDuration(int paymentTermDuration) {
        this.paymentTermDuration = paymentTermDuration;
    }

    public List<com.lexofficeinvoice.model.PaymentDiscountConditions> getPaymentDiscountConditions() {
        return paymentDiscountConditions;
    }

    public void setPaymentDiscountConditions(List<com.lexofficeinvoice.model.PaymentDiscountConditions> paymentDiscountConditions) {
        this.paymentDiscountConditions = paymentDiscountConditions;
    }

    @Override
    public String toString() {
        return "PaymentConditions{" +
                "paymentTermLabel='" + paymentTermLabel + '\'' +
                ", paymentTermDuration=" + paymentTermDuration +
                ", paymentDiscountConditions=" + paymentDiscountConditions +
                '}';
    }
}
