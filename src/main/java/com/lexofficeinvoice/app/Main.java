package com.lexofficeinvoice.app;

import com.google.gson.Gson;
import com.lexofficeinvoice.model.*;
//import com.stripe.Stripe;
import com.lexofficeinvoice.model.Address;
import com.stripe.Stripe;
import com.stripe.exception.StripeException;

import com.stripe.model.BalanceTransaction;
import com.stripe.model.BalanceTransactionCollection;
import com.stripe.model.Payout;
import com.stripe.model.PayoutCollection;
import com.stripe.util.StringUtils;
import io.github.cdimascio.dotenv.Dotenv;
import okhttp3.*;

import java.io.IOException;
import java.math.BigDecimal;
import java.sql.*;
import java.sql.Connection;
import java.text.*;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.ZoneId;
import java.time.ZoneOffset;
import java.util.*;
import java.util.Date;
import java.util.regex.Pattern;

public class Main {

    public static void main(String[] args) {

        //Set to first day of the month and convert it to Unix Timestamp
        LocalDate todayDate = LocalDate.now();
        LocalDate endMonthDate = LocalDate.now();

        int daysOfMonth = Calendar.getInstance().getActualMaximum(Calendar.DAY_OF_MONTH); //get days of the current month

        todayDate = todayDate.withDayOfMonth(1);
        endMonthDate = endMonthDate.withDayOfMonth(daysOfMonth);

        todayDate = todayDate.withDayOfMonth(1);


        //default time zone
        ZoneId defaultZoneId = ZoneId.systemDefault();
        Date dateStartOfMonth = Date.from(todayDate.atStartOfDay(defaultZoneId).toInstant());
        Date dateEndOfMonth = Date.from(endMonthDate.atStartOfDay(defaultZoneId).toInstant());


        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.MONTH, -1);
        cal.set(Calendar.DATE, 1);
        Date firstDateOfPreviousMonth = cal.getTime();

        cal.set(Calendar.DATE, cal.getActualMaximum(Calendar.DATE));

        Date lastDateOfPreviousMonth = cal.getTime();

        int year = todayDate.getMonthValue() == 1 ? todayDate.getYear() - 1 : todayDate.getYear();
        int month = todayDate.getMonthValue() - 1 == 0 ? 12 : todayDate.getMonthValue() - 1;

        LocalDate initial = LocalDate.of(year, month, 1);
        LocalDate initial2 = LocalDate.of(year, month, cal.getActualMaximum(Calendar.DAY_OF_MONTH));


        Payout payout = null;
        // BalanceTransaction bt = null;
        BalanceTransactionCollection balanceTransactions = null;
        Dotenv dotenv = Dotenv.load();
        Stripe.apiKey = dotenv.get("STRIPE_API_KEY");

        String pattern = "yyyy-MM-dd'T'HH:mm:ss.SSSXXX";
        SimpleDateFormat simpleDateFormat = new SimpleDateFormat(pattern, new Locale("de", "DE"));


        long unixTimestamp = LocalDate.parse(initial.toString()).toEpochSecond(LocalTime.MIDNIGHT, ZoneOffset.UTC);
        long unixTimestampEndMonth = LocalDate.parse(initial2.toString()).toEpochSecond(LocalTime.MIDNIGHT, ZoneOffset.UTC);


        Map<String, LexInvoice> lexInvoiceMap = new HashMap<>();
        Map<String, Long> accountPricesMap = new HashMap<>();
        long arrivalDateStart = 0L;
        long arrivalDateLatest = 0L;
        ResultSet subsidiaries = null;
        ResultSet subsidiariesAddresses = null;
        PayoutCollection payouts = null;

        try {
            Class.forName("com.mysql.cj.jdbc.Driver");
            Connection con = DriverManager.getConnection(
                    "jdbc:mysql://" + dotenv.get("DB_HOST") + ":3306/", "" + dotenv.get("DB_USERNAME"), "" + dotenv.get("DB_PW"));
            System.out.println("Connected to the database");

            Statement stmt = con.createStatement();
            Statement stmt2 = con.createStatement();
            Statement stmt3 = con.createStatement();
            subsidiaries = stmt.executeQuery("SELECT * FROM production.subsidiaries;");
            subsidiariesAddresses = stmt2.executeQuery("SELECT * from production.addresses where subsidiary_id is not null ;");
            //  System.out.println(subsidiaries.getMetaData().getColumnName(1));
        } catch (SQLException | ClassNotFoundException e) {
            e.printStackTrace();
        }
//1606780800 1/12/2020
        try {
            Map<String, Object> paramsPayout = new HashMap<>();
            paramsPayout.put("limit", 35);
            paramsPayout.put("arrival_date[gte]", unixTimestamp);
            paramsPayout.put("arrival_date[lte]", unixTimestampEndMonth);
            paramsPayout.put("status", "paid");

            payouts = Payout.list(paramsPayout);


            arrivalDateStart = payouts.getData().get(0).getArrivalDate();
            arrivalDateLatest = payouts.getData().get(payouts.getData().size() - 1).getArrivalDate();
            if (payouts.getData().size() == 1) {
                paramsPayout.put("limit", 2);


                paramsPayout.put("arrival_date[gte]", unixTimestamp - 2678400);
                paramsPayout.put("arrival_date[lte]", unixTimestampEndMonth);
                paramsPayout.put("status", "paid");
                payouts = Payout.list(paramsPayout);
                arrivalDateStart = payouts.getData().get(0).getArrivalDate();
                arrivalDateLatest = payouts.getData().get(payouts.getData().size() - 1).getArrivalDate();
            }


            System.out.println(payouts.getData());
        } catch (StripeException exception) {

            System.out.println(exception.getMessage());

        }
        int counter = 0;
        for (Payout payout1 : payouts.getData()) {


            Map<String, Object> paramsBalance = new HashMap<>();
            paramsBalance.put("limit", 1000);
            paramsBalance.put("type", "application_fee");
            paramsBalance.put("payout", payout1.getId());


            try {
                if (counter == 0) {
                    balanceTransactions = BalanceTransaction.list(paramsBalance);
                } else {
                    for (BalanceTransaction bt : BalanceTransaction.list(paramsBalance).getData()) {
                        balanceTransactions.getData().add(bt);
                    }
                }

                counter++;

            } catch (StripeException e) {
                e.printStackTrace();
            }
        }

        System.out.println(balanceTransactions.getData().size());

        System.out.println(arrivalDateStart + " und " + arrivalDateLatest);
        int count = 0;
        for (BalanceTransaction bt : balanceTransactions.getData()) {

            String description = bt.getDescription();
            int firstIndex = description.indexOf('(');
            int lastIndex = description.indexOf(')');
            String account_id = "";

            System.out.println(description);
            if (!description.contains("STRIPE")) {
                account_id = description.substring(firstIndex + 1, lastIndex);
            }

            if (accountPricesMap.containsKey(account_id)) {

                accountPricesMap.put(account_id, bt.getAmount() + accountPricesMap.get(account_id));
                System.out.println(account_id + " " + bt.getAmount());


            } else {
                accountPricesMap.put(account_id, bt.getAmount());
                System.out.println(account_id + " " + bt.getAmount());

            }
            count++;

        }
        System.out.println(count);


        try {
            int i = 0;
            while (subsidiaries.next()) {

                String stripe_id = subsidiaries.getString("stripe_id");
                int subsidiary_id = subsidiaries.getInt("id");

                if (accountPricesMap.containsKey(stripe_id)) {
                    while (subsidiariesAddresses.next()) {
                        int subsidiary_address_id = subsidiariesAddresses.getInt("subsidiary_id");
                        if (subsidiary_address_id == subsidiary_id) {
                            String tempName = subsidiaries.getString("name");
                            String tempId = subsidiaries.getString("id");


                            String tempStreet = subsidiariesAddresses.getString("street");
                            String tempCity = subsidiariesAddresses.getString("city");
                            int tempZip = subsidiariesAddresses.getInt("postal_code");
                            String tempSupplement = subsidiariesAddresses.getString("house_number");
                            double taxes = (accountPricesMap.get(stripe_id) / 100.00) / 119.00 * 19.00;
                            //double netPrice = ((accountPricesMap.get(stripe_id)/100.00) * 100) / 119;

                            double grossPrice = accountPricesMap.get(stripe_id) / 100.00;
                            double netPrice = grossPrice - taxes;
                            System.out.println(netPrice);

                            //  double taxes = grossPrice-netPrice;


                            // String taxesString = new DecimalFormat("#.00", DecimalFormatSymbols.getInstance()).format(round(grossPrice - netPrice, 2));

                            //System.out.println(taxesString);
                            //taxesString = taxesString.replace(',', '.');
                            //double taxes = Double.parseDouble(taxesString);
                            System.out.println(taxes);

                            //System.out.println(taxes +netPrice);
                            // System.out.println(taxes);
                            if (!lexInvoiceMap.containsKey(tempName)) {

                                //Create new address
                                Address address = new Address();
                                address.setContactId(null);
                                address.setName(tempName);
                                if (tempSupplement == "" || tempSupplement == null)
                                    address.setStreet(tempStreet);
                                else address.setStreet(tempStreet + " " + tempSupplement);

                                address.setCity(tempCity);
                                address.setZip(tempZip);
                                address.setCountryCode(Address.CountryCode.DE);

                                //Create Unitprice for LineItems
                                UnitPrice.Currency currency = UnitPrice.Currency.EUR;
                                UnitPrice unitPrice = new UnitPrice();
                                unitPrice.setNetAmount(round(netPrice,2));
                                //unitPrice.setGrossAmount(grossPrice);
                                unitPrice.setTaxRatePercentage(19);
                                unitPrice.setCurrency(currency);

                                //Set LineItems
                                LineItems.Type type = LineItems.Type.custom;
                                List<LineItems> lineItemList = new ArrayList<>();
                                LineItems lineItems = new LineItems();
                                lineItems.setId(null);
                                lineItems.setName("Plattformgebühr");
                                lineItems.setType(type);
                                lineItems.setDescription("");
                                lineItems.setQuantity(1);
                                lineItems.setUnitPrice(unitPrice);
                                lineItems.setLineItemAmount(1);
                                lineItems.setUnitName("Plattformgebühr");
                                //lineItems.setDiscountPercentage(0);

                                lineItemList.add(lineItems);

                                //Create TotalPrice
                                TotalPrice totalPrice = new TotalPrice();
                                totalPrice.setCurrency("EUR");
                                totalPrice.setTotalNetAmount(round(netPrice,2));
                                totalPrice.setTotalTaxAmount(round(taxes,2));
                                totalPrice.setTotalGrossAmount(round(grossPrice,2));
                                //totalPrice.setTotalDiscountAbsolute(0);
                                // totalPrice.setTotalDiscountPercentage(0);

                                //Create TaxCondition
                                TaxConditions.TaxType taxType = TaxConditions.TaxType.net;
                                TaxConditions taxConditions = new TaxConditions();
                                taxConditions.setTaxTypeNote(null);
                                taxConditions.setTaxType(taxType);

                                //Create TaxAmount list
                                List<TaxAmount> taxAmountList = new ArrayList<>();
                                TaxAmount taxAmount = new TaxAmount();
                                taxAmount.setTaxRatePercentage(19);
                                taxAmount.setNetAmount(round(netPrice,2));
                                taxAmount.setTaxAmount(round(taxes,2));

                                taxAmountList.add(taxAmount);


                                //Create PaymentCondition
                                PaymentConditions paymentConditions = new PaymentConditions();
                                paymentConditions.setPaymentTermDuration(7);
                                //paymentConditions.setPaymentDiscountConditions(null);
                                paymentConditions.setPaymentTermLabel("Die Rechnung wurde bereits beglichen.");

                                //Create ShippingCondition
                                ShippingConditions.ShippingType shippingType = ShippingConditions.ShippingType.serviceperiod;
                                ShippingConditions shippingConditions = new ShippingConditions();
                                /*shippingConditions.setShippingDate(simpleDateFormat.format(dateStartOfMonth));
                                shippingConditions.setShippingEndDate(simpleDateFormat.format(dateEndOfMonth));*/
                                shippingConditions.setShippingDate(simpleDateFormat.format(firstDateOfPreviousMonth));
                                shippingConditions.setShippingEndDate(simpleDateFormat.format(lastDateOfPreviousMonth));
                              /*  shippingConditions.setShippingDate("2020-09-01T00:00:00.000+02:00");
                                shippingConditions.setShippingEndDate("2020-09-30T00:00:00.000+02:00");*/
                                shippingConditions.setShippingType(shippingType);


                                java.util.Date dateNow = new java.util.Date();

                                // System.out.println("Ich erstelle neue Invoice Object");

                                java.util.Date createdDate = new Date((arrivalDateLatest * 1000)); //getData().get(index)
                                LexInvoice lexInvoice = new LexInvoiceBuilder(tempId)
                                        //.setOrganizationId(null)
                                        //.setCreatedDate(simpleDateFormat.format(createdDate))
                                        //.setUpdatedDate(simpleDateFormat.format(dateNow))
                                        //.setVersion(1)
                                        //.setLanguage("de")
                                        .setArchived(false)
                                        .setVoucherStatus(LexInvoice.VoucherStatus.draft)
                                        //.setVoucherNumber(null)
                                        .setVoucherDate(simpleDateFormat.format(dateNow))
                                        .setAddress(address)
                                        .setLineItems(lineItemList)
                                        .setTotalPrice(totalPrice)
                                        .setTaxAmounts(taxAmountList)
                                        .setTaxConditions(taxConditions)
                                        .setPaymentConditions(paymentConditions)
                                        .setShippingConditions(shippingConditions)
                                        .setTitle("Rechnung")
                                        .setIntroduction("Sehr geehrte Damen und Herren,\nvielen Dank für Ihr Vertrauen in FAAREN. Hiermit stellen wir Ihnen die folgende Leistung in Rechnung.")
                                        .setRemark("\nMit freundlichen Grüßen\nIhr Team der Faaren GmbH")
                                        .build();


                                //Add new LexInvoice object to Map
                                lexInvoiceMap.put(tempName, lexInvoice);


                            }
                        }
                    }
                    subsidiariesAddresses.beforeFirst();

                }
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }


        System.out.println(accountPricesMap);


        ArrayList<LexInvoice> valueList = new ArrayList<LexInvoice>(lexInvoiceMap.values());
        // System.out.println(valueList);
        Gson gson = new Gson();
        //loop through every entry and post it to Lexoffice
        for (int i = 0; i < valueList.size(); i++) {

            String json = gson.toJson(valueList.get(i));
            json.replace("LexInvoice", "");
            System.out.println(json);
            sendInvoiceToLex(json);

        }

        //System.out.println(listOfFees);

    }


    public static void sendInvoiceToLex(String json) {

        Dotenv dotenv = Dotenv.load();

        try {
            OkHttpClient client = new OkHttpClient().newBuilder()
                    .build();
            MediaType mediaType = MediaType.parse("application/json");
            RequestBody body = RequestBody.create(mediaType, json);
            Request request = new Request.Builder()
                    .url("https://api.lexoffice.io/v1/invoices")
                    .method("POST", body)
                    .addHeader("Authorization", "Bearer " + dotenv.get("LEXOFFICE_API"))
                    .addHeader("Accept", "application/json")
                    .addHeader("Content-Type", "application/json")
                    .build();
            Response response = client.newCall(request).execute();
            System.out.println(response);
            response.body().close();

        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static double round(double value, int decimalPoints) {

        double d = Math.pow(10, decimalPoints);
        return (Math.round((value - 0.001) * d) / d);

    }
}


