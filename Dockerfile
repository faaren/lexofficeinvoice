FROM openjdk:15
COPY ./out/artifacts/LexofficeInvoice_jar/LexofficeInvoice.jar /usr/app/
COPY .env /usr/app
WORKDIR /usr/app
ENTRYPOINT ["java","-jar","LexofficeInvoice.jar"]